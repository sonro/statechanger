<?php

abstract class CronJson
{
    /**
     * To Array.
     *
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * To JSON.
     *
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }

    /**
     * Set Data Property.
     *
     * @param array  $data
     * @param string $property
     * @param $default
     */
    protected function setDataProperty($data, $property, $default)
    {
        $this->$property = isset($data[$property]) ? $data[$property] : $default;
    }
}

