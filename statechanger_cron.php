<?php

chdir(dirname(__FILE__));
include(dirname(__FILE__) . '/../../config/config.inc.php');
include(dirname(__FILE__) . '/../../init.php');
include(dirname(__FILE__) . '/CronJson/CronJsonResponse.php');
include(dirname(__FILE__) . '/CronJson/CronJsonError.php');
include(dirname(__FILE__) . '/tools/ScCronJob.php');
include(dirname(__FILE__) . '/tools/Changer.php');

header('Content-Type: application/json');
$cronJsonResponse = new CronJsonResponse();

$job = Tools::getValue('set', 0);

if (substr(_COOKIE_KEY_, 34, 8) != Tools::getValue('token')) {
    $cronJsonResponse->errors[] = new CronJsonError([
        'title' => 'statechanger: Invalid token',
        'code' => 1,
        'detail' => 'Incorrect token supplied to statechanger cron job',
    ]);
} else if (!$job) {
    $cronJsonResponse->errors[] = new CronJsonError([
        'title' => 'statechanger: Invalid job',
        'code' => 2,
        'detail' => 'Incorrect "set" value supplied to statechanger cron job',
    ]);
} else {
    switch ($job) {
        case ScCronJob::JOB_CUSTOM:
            if (!Configuration::get('STATE_CHANGER_CRON_ON')) {
                $cronJsonResponse->errors[] = new CronJsonError([
                    'title' => 'statechanger: Custom cron job not set',
                    'code' => 3,
                    'detail' => 'Go to module config and setup custom cron job',
                ]);
                break;
            }
            $init_status = Configuration::get('STATE_CHANGER_CRON_INIT_STATUS');
            $target_status = Configuration::get('STATE_CHANGER_CRON_TARGET_STATUS');
            $trigger_category = Configuration::get('STATE_CHANGER_CRON_TRIGGER_CATEGORY');
            $statuses_changed = Changer::manProcess($init_status, $target_status, $trigger_category);
            $cronJsonResponse->executed = true;
            if ($statuses_changed < 0) {
                $cronJsonResponse->errors[] = new CronJsonError([
                    'title' => 'statechanger: Custom cron job failed',
                    'code' => 4,
                    'detail' => "Failed changing orders with product category '"
                                .$trigger_category
                                ."' from '$init_status' to '$target_status'",
                ]);
                break;
            }
            $cronJsonResponse->successful = true;
            // wipe off 'orders todo array'
            $new_orders = array();
            Configuration::updateValue('STATE_CHANGER_NEW_ORDERS', serialize($new_orders));
            $cronJsonResponse->result[] = $statuses_changed;
            break;
        
        case ScCronJob::JOB_ACCEPT_TO_PREP:
            $init_status = Configuration::get('STATE_CHANGER_BASIC_ACCEPT');
            $target_status = Configuration::get('STATE_CHANGER_BASIC_PREP');
            $statuses_changed = Changer::basicProcess($init_status, $target_status);
            $cronJsonResponse->executed = true;
            if ($statuses_changed < 0) {
                $cronJsonResponse->errors[] = new CronJsonError([
                    'title' => 'statechanger: Basic cron job failed',
                    'code' => 5,
                    'detail' => "Failed changing orders from '$init_status' to '$target_status'",
                ]);
                break;
            }
            $cronJsonResponse->successful = true;
            $cronJsonResponse->result[] = $statuses_changed;
            break;

        case ScCronJob::JOB_PREP_TO_SHIPPED:
            $init_status = Configuration::get('STATE_CHANGER_BASIC_PREP');
            $target_status = Configuration::get('STATE_CHANGER_BASIC_SHIPPED');
            $statuses_changed = Changer::basicProcess($init_status, $target_status);
            $cronJsonResponse->executed = true;
            if ($statuses_changed < 0) {
                $cronJsonResponse->errors[] = new CronJsonError([
                    'title' => 'statechanger: Basic cron job failed',
                    'code' => 6,
                    'detail' => "Failed changing orders from '$init_status' to '$target_status'",
                ]);
                break;
            }
            $cronJsonResponse->successful = true;
            $cronJsonResponse->result[] = $statuses_changed;
            break;

        case ScCronJob::JOB_SHIPPED_TO_DELIVERED:
            $init_status = Configuration::get('STATE_CHANGER_BASIC_SHIPPED');
            $target_status = Configuration::get('STATE_CHANGER_BASIC_DELIVERED');
            $statuses_changed = Changer::deliveredProcess($init_status, $target_status);
            $cronJsonResponse->executed = true;
            if ($statuses_changed < 0) {
                $cronJsonResponse->errors[] = new CronJsonError([
                    'title' => 'statechanger: Basic cron job failed',
                    'code' => 7,
                    'detail' => "Failed changing orders from '$init_status' to '$target_status'",
                ]);
                break;
            }
            $cronJsonResponse->successful = true;
            $cronJsonResponse->result[] = $statuses_changed;
            break;

        default:
            $cronJsonResponse->errors[] = new CronJsonError([
                'title' => 'statechanger: Unknown error',
                'code' => 8,
                'detail' => "",
            ]);
            break;
    }
} 

echo $cronJsonResponse->toJson();
