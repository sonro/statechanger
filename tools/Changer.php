<?php

class Changer
{
    public static function orderHasTrigger($id, $category_trigger)
    {
        $order = new Order($id); 
        $products = $order->getProducts();
        foreach ($products as $product) {
            if ($product['id_category_default'] == $category_trigger) {
                return true;
            }
        }
        return false;
    }

    public static function manProcess($init_status, $target_status, $category_trigger)
    {
        $order_ids = self::getOrderIdsWithState($init_status);
        $processed_orders = 0;

        // search orders for any products containing category trigger
        foreach ($order_ids as $id) {
            if (self::orderHasTrigger($id, $category_trigger)) {
                if (self::updateStatus($id, $target_status)) {
                    $processed_orders++;
                } else {
                    $processed_orders = -1;
                    break;
                }

            }
        }

        return $processed_orders;
    }

    public static function updateStatus($id_order, $target_status)
    {
        $history = new OrderHistory();
        $history->id_order = $id_order;
        $history->changeIdOrderState($target_status, $id_order);
        return $history->addWithemail(true);
    }

    public static function getOrderIdsWithState($init_status)
    {
        // get order ids where order state matches init status from database
        $sql = 'SELECT id_order
				FROM '._DB_PREFIX_.'orders o
				WHERE o.`current_state` = '.(int)$init_status
				.Shop::addSqlRestriction(false, 'o');
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        $order_ids = array();
        foreach ($result as $order) {
            $order_ids[] = (int)$order['id_order'];
        }

        return $order_ids;
    }

    public static function basicProcess($init_status, $target_status)
    {
        $order_ids = self::getOrderIdsWithState($init_status);
        $processed_orders = 0;
        foreach ($order_ids as $id) {
            if (self::updateStatus($id, $target_status)) {
                $processed_orders++;
            } else {
                $processed_orders = -1;
                break;
            }
        }

        return $processed_orders;
    }

    public static function deliveredProcess($init_status, $target_status)
    {
        $order_ids = self::getOrderIdsWithState($init_status);
        $processed_orders = 0;
        $ukSince = strtotime('-7 days');
        $otherSince = strtotime('-15 days');
        foreach ($order_ids as $id) {
            if (self::orderOldEnough($id, $init_status, $ukSince, $otherSince)) {
                if (self::updateStatus($id, $target_status)) {
                    $processed_orders++;
                } else {
                    $processed_orders = -1;
                    break;
                }
            }
        }

        return $processed_orders;
    }

    public static function orderOldEnough($id, $state, $ukSince, $otherSince) {
        $sql = 'SELECT h.id_order_history, h.date_add
				FROM '._DB_PREFIX_.'order_history h
                WHERE h.`id_order_state` = '.(int)$state.'
                AND h.`id_order` = '.(int)$id;
        $result = Db::getInstance()->executeS($sql);
        $mostRecentChange = strtotime($result[0]['date_add']);
        foreach ($result as $history) {
            $changeAt = strtotime($history['date_add']);
            if ($changeAt > $mostRecentChange) {
                $mostRecentChange = $changeAt;
            }
        }

        $order = new Order($id); 
        $zone = Address::getZoneById($order->id_address_delivery);
        $since = $zone == 1 ? $ukSince : $otherSince;
        if ($mostRecentChange < $since) {
            return true;
        }
        return false;
    }
}
