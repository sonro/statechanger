<?php

class ScCronJob
{
    const JOB_CUSTOM = 1;
    const JOB_ACCEPT_TO_PREP = 2;
    const JOB_PREP_TO_SHIPPED = 3;
    const JOB_SHIPPED_TO_DELIVERED = 4;

    private static $titles = array(
        self::JOB_CUSTOM => 'Custom StateChanger CronJob',
        self::JOB_ACCEPT_TO_PREP => 'Move all Payment Accepted orders to Processing in Progress',
        self::JOB_PREP_TO_SHIPPED => 'Move all Preperation in Progress orders to Shipped',
        self::JOB_SHIPPED_TO_DELIVERED => 'Move old Shipped orders to Delivered',
    );

    public static function getLinks($link_base, $link_suffix)
    {
        $links = array();
        foreach (self::$titles as $job => $title) {
            $link = $link_base.$job.$link_suffix;
            $links[$job] = array(
                'title' => $title,
                'link' => $link,
            );
        }

        return $links;
    }

}
