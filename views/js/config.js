$(document).ready(function() {
    let enableManOn = document.getElementById('enable_man_on');
    let enableManOff = document.getElementById('enable_man_off');
    let enableAutoOn = document.getElementById('enable_auto_on');
    let enableAutoOff = document.getElementById('enable_auto_off');
    let enableCronOn = document.getElementById('enable_cron_on');
    let enableCronOff = document.getElementById('enable_cron_off');

    if (enableManOn.checked) {
        formShow('man', true);
    } else {
        formShow('man', false);
    }

    if (enableAutoOn.checked) {
        formShow('auto', true);
    } else {
        formShow('auto', false);
    }

    if (enableCronOn.checked) {
        formShow('cron', true);
    } else {
        formShow('cron', false);
    }

    enableManOn.onclick = function() { formShow('man', true); };
    enableManOff.onclick = function() { formShow('man', false); };
    enableAutoOn.onclick = function() { formShow('auto', true); };
    enableAutoOff.onclick = function() { formShow('auto', false); };
    enableCronOn.onclick = function() { formShow('cron', true); };
    enableCronOff.onclick = function() { formShow('cron', false); };

    // copy cron-link url
    // document.getElementById("cron-link-button").addEventListener("click", function() {
    //     let copyText = document.getElementById("cron-link-text").innerHTML;

    //     let target = document.createElement("textarea");
    //     target.style.position = "absolute";
    //     target.style.left = "-9999px";
    //     target.style.top = "0";
    //     target.id = "_hiddenCopyText_";
    //     document.body.appendChild(target);

    //     target.textContent = copyText;

    //     target.select();
    //     document.execCommand("copy");
    // });
});

function formShow(form, show)
{
    let id = '#' + form + 'statechanger_form';
    let thisForm = $(id + ' .form-group:not(:first)');
    if (show) {
        thisForm.show();
    } else {
        thisForm.hide();
    }

}
