<div class="panel">
    <div class="panel-heading">
        <i class="icon-tasks"></i>
        Cron Links
    </div>
    <div>
        <ul>
            {foreach from=$cron_links item=job}
                <li>
                   <h4 class="cron-link-title">{$job['title']}</h4>
                   <p class="cron-link-url">{$job['link']}</p>
                </li>
           {/foreach}
       </ul>
    </div>
    <!-- <div class="panel-footer"> -->
    <!--     <button id="cron-link-button" class="btn btn-default pull-right"> -->
    <!--         <i class="icon-copy"></i> -->
    <!--         Copy -->
    <!--     </button> -->
    <!-- </div> -->
</div>
