{if isset($statuses_changed)}
    {if $statuses_changed > 0}
        <div class="alert alert-success">{$statuses_changed} Orders Status Changed!</div>
    {elseif $statuses_changed == 0}
        <div class="alert alert-info">No Orders Processed</div>
    {else}
        <div class="alert alert-danger">Error</div>
    {/if}
{/if}
<style>
    .help-setting {
        color: #000;
        font-weight: bold;
    }
</style>
{if $man_enabled}
<div class="panel">
    <div class="panel-heading">
        <legend>Manual Bulk Update</legend>
    </div>
    <form action="" method="post">
        <input class="btn btn-default btn-lg" type="submit" name="sc_action" value="Change Order States" />
        <p class="help-block">Change <span class="help-setting">{$init_status}</span> to <span class="help-setting">{$target_status}</span> if order has
        products of default category <span class="help-setting">{$trigger_category}</span></p>
    </form>
</div>
{/if}
<div class="panel">
    <a class="btn btn-default" href="{$settings_link}">Settings</a>
</div>
    

