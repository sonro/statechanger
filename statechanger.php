<?php
if (!defined('_PS_VERSION_')) {
      exit;
}

include_once (_PS_MODULE_DIR_ . '/statechanger/tools/Changer.php');
include_once (_PS_MODULE_DIR_ . '/statechanger/tools/ScCronJob.php');

class StateChanger extends Module
{
    public function __construct()
    {
        $this->name = 'statechanger';
        $this->tab = 'quick_bulk_update';
        $this->version = '1.0';
        $this->author = 'Christopher Morton';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_); 
        $this->bootstrap = true;
        $this->path = __PS_BASE_URI__.'modules/statechanger/';

        parent::__construct();

        $this->displayName = $this->l('State Changer');
        $this->description = $this->l('Update the status of orders which contain products of a certain category. Either at point of sale, or by bulk update in back office or cron job');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
}

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        $ps_states = OrderState::getOrderStates(1);
        foreach ($ps_states as $state){
            if ($state['name'] == 'Payment accepted') {
                $states['accept'] = (int)$state['id_order_state'];
            } else if ($state['name'] == 'Processing in progress') {
                $states['prep'] = (int)$state['id_order_state'];
            } else if ($state['name'] == 'Shipped') {
                $states['shipped'] = (int)$state['id_order_state'];
            } else if ($state['name'] == 'Delivered') {
                $states['delivered'] = (int)$state['id_order_state'];
            }
        }


        if (parent::install()
            && Configuration::updateValue('STATE_CHANGER_AUTO_ON', false)
            && Configuration::updateValue('STATE_CHANGER_MAN_ON', false)
            && Configuration::updateValue('STATE_CHANGER_CRON_ON', false)
            && Configuration::updateValue('STATE_CHANGER_MAN_INIT_STATUS', 2)
            && Configuration::updateValue('STATE_CHANGER_MAN_TARGET_STATUS', 2)
            && Configuration::updateValue('STATE_CHANGER_MAN_TRIGGER_CATEGORY', 2)
            && Configuration::updateValue('STATE_CHANGER_CRON_INIT_STATUS', 2)
            && Configuration::updateValue('STATE_CHANGER_CRON_TARGET_STATUS', 2)
            && Configuration::updateValue('STATE_CHANGER_CRON_TRIGGER_CATEGORY', 2)
            && Configuration::updateValue('STATE_CHANGER_AUTO_TARGET_STATUS', 2)
            && Configuration::updateValue('STATE_CHANGER_AUTO_TRIGGER_CATEGORY', 2)
            && Configuration::updateValue('STATE_CHANGER_NEW_ORDERS', serialize(array()))
            && Configuration::updateValue('STATE_CHANGER_BASIC_ACCEPT', $states['accept'])
            && Configuration::updateValue('STATE_CHANGER_BASIC_PREP', $states['prep'])
            && Configuration::updateValue('STATE_CHANGER_BASIC_SHIPPED', $states['shipped'])
            && Configuration::updateValue('STATE_CHANGER_BASIC_DELIVERED', $states['delivered'])
            && $this->installTab('AdminOrders', 'AdminStateChanger', 'State Changer')
            && $this->registerHook('actionOrderHistoryAddAfter')
            && $this->registerHook('footer')
        ) {
            return true;
        }

        return false;
    }

    public function uninstall()
    {
        if (parent::uninstall()
            && Configuration::deleteByName('STATE_CHANGER_MAN_ON')
            && Configuration::deleteByName('STATE_CHANGER_AUTO_ON')
            && Configuration::deleteByName('STATE_CHANGER_CRON_ON')
            && Configuration::deleteByName('STATE_CHANGER_MAN_INIT_STATUS')
            && Configuration::deleteByName('STATE_CHANGER_MAN_TARGET_STATUS')
            && Configuration::deleteByName('STATE_CHANGER_MAN_TRIGGER_CATEGORY')
            && Configuration::deleteByName('STATE_CHANGER_CRON_INIT_STATUS')
            && Configuration::deleteByName('STATE_CHANGER_CRON_TARGET_STATUS')
            && Configuration::deleteByName('STATE_CHANGER_CRON_TRIGGER_CATEGORY')
            && Configuration::deleteByName('STATE_CHANGER_AUTO_TARGET_STATUS')
            && Configuration::deleteByName('STATE_CHANGER_AUTO_TRIGGER_CATEGORY')
            && Configuration::deleteByName('STATE_CHANGER_NEW_ORDERS')
            && Configuration::deleteByName('STATE_CHANGER_BASIC_ACCEPT')
            && Configuration::deleteByName('STATE_CHANGER_BASIC_PREP')
            && Configuration::deleteByName('STATE_CHANGER_BASIC_SHIPPED')
            && Configuration::deleteByName('STATE_CHANGER_BASIC_DELIVERED')
            && $this->uninstallTab('AdminStateChanger')
        ) {
            return true;
        }

        return false;
    }

    private function installTab($parent, $class_name, $name)
    {
        $tab = new Tab();
        $tab->id_parent = (int)Tab::getIdFromClassName($parent);
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $name;
        }
        $tab->class_name = $class_name;
        $tab->module = $this->name;
        $tab->active = 1;

        return $tab->add();
    }

    private function uninstallTab($class_name)
    {
        $id_tab = (int)Tab::getIdFromClassName($class_name);
        $tab = new Tab($id_tab);

        return $tab->delete();
    }

    public function hookFooter($params)
    {
        if (Configuration::get('STATE_CHANGER_AUTO_ON')) 
        {
            $new_orders = unserialize(Configuration::get('STATE_CHANGER_NEW_ORDERS'));
            $target_state = Configuration::get('STATE_CHANGER_AUTO_TARGET_STATUS');
            foreach ($new_orders as $key => $order_id) {
                $order = new Order($order_id);
                if ($order->current_state == Configuration::get('PS_OS_PAYMENT')) {
                    Changer::updateStatus($order_id, $target_state);
                } 

                unset($new_orders[$key]); 
            }
            Configuration::updateValue('STATE_CHANGER_NEW_ORDERS', serialize($new_orders));
        }
    }

    public function hookActionOrderHistoryAddAfter($params)
    {
        $new_os = $params['order_history'];
        if (Configuration::get('STATE_CHANGER_AUTO_ON')
            && $new_os->id_order_state == Configuration::get('PS_OS_PAYMENT')
        ) {
            if (Changer::orderHasTrigger(
                $new_os->id_order,
                Configuration::get('STATE_CHANGER_AUTO_TRIGGER_CATEGORY')
            )) {
                $new_orders = unserialize(Configuration::get('STATE_CHANGER_NEW_ORDERS'));
                array_push($new_orders, $new_os->id_order);
                Configuration::updateValue('STATE_CHANGER_NEW_ORDERS', serialize($new_orders));
            }
        }
    }

    public function getContent()
    {
        $output = '';

        $config_result = $this->processConfiguration();

        if ($config_result < 0) {
            $output .= $this->displayError($this->l('Invalid Configuration value'));
        } elseif ($config_result > 0) {
            $output .= $this->displayConfirmation($this->l('Settings updated'));
        }

        $cron_link_base = Tools::getHttpHost(true)
            .__PS_BASE_URI__
            .'modules/statechanger/statechanger_cron.php?set=';
        $cron_link_suffix = '&token='.substr(_COOKIE_KEY_, 34, 8);

        $cron_links = ScCronJob::getLinks($cron_link_base, $cron_link_suffix);
        if (!Configuration::get('STATE_CHANGER_CRON_ON')) {
            array_shift($cron_links);
        }

        $this->context->smarty->assign(array(
            'cron_links' => $cron_links,
        ));

        $this->context->controller->addJS($this->path.'views/js/config.js');
        $output .= $this->display(__FILE__, 'getContent.tpl');
        $output .= $this->renderForms();

        return $output;
    }


    public function renderForms()
    {
        $states = OrderState::getOrderStates($this->context->language->id);
        $categories = Category::getSimpleCategories($this->context->language->id);
        usort($categories, function($a, $b) {
                return $a['name'] - $b['name'];
        });

        $man_fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Manual Bulk Update Configuration'),
                    'icon' => 'icon-cog'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow Manual Update'),
                        'name' => 'enable_man',
                        'desc' => $this->l('Enable back office bulk update of orders'),
                        'values' => array(
                            array(
                                'id' => 'enable_man_update_1',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ), array(
                                'id' => 'enable_man_update_0',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            ),
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Initial Order State'),
                        'name' => 'man_init_status',
                        'desc' => $this->l('Order state to search through for the trigger category.'),
                        'options' => array(
                            'query' => $states,
                            'id' => 'id_order_state',
                            'name' => 'name',
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Target Order State'),
                        'name' => 'man_target_status',
                        'desc' => $this->l('Order state to change to if the trigger category is found.'),
                        'options' => array(
                            'query' => $states,
                            'id' => 'id_order_state',
                            'name' => 'name',
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Trigger Category'),
                        'name' => 'man_trigger_category',
                        'desc' => $this->l('Default product category that will trigger status change.'),
                        'options' => array(
                            'query' => $categories, 
                            'id' => 'id_category',
                            'name' => 'name',
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );

        $man_helper = new HelperForm();
        $man_helper->table = 'manstatechanger';
        $man_helper->default_form_languer =
            (int)Configuration::get('PS_LANG_DEFUALT');
        $man_helper->allow_employee_form_lang =
            (int)Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG');
        $man_helper->submit_action = 'sc_man_config_form';
        $man_helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $man_helper->token = Tools::getAdminTokenLite('AdminModules');
        $man_helper->tpl_vars = array(
            'fields_value' => array(
                'enable_man' => Tools::getValue('enable_man',
                    Configuration::get('STATE_CHANGER_MAN_ON')),
                'man_init_status' => Tools::getValue('man_init_status',
                    Configuration::get('STATE_CHANGER_MAN_INIT_STATUS')),
                'man_target_status' => Tools::getValue('man_target_status',
                    Configuration::get('STATE_CHANGER_MAN_TARGET_STATUS')),
                'man_trigger_category' => Tools::getValue('man_trigger_category',
                    Configuration::get('STATE_CHANGER_MAN_TRIGGER_CATEGORY')),
                'languages' => $this->context->controller->getLanguages(),
            ),
        );
        
        $auto_fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Automatic Configuration'),
                    'icon' => 'icon-cog'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow Automatic Update'),
                        'name' => 'enable_auto',
                        'desc' => $this->l('Enable order update to target status upon payment accept'),
                        'values' => array(
                            array(
                                'id' => 'enable_auto_update_1',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ), array(
                                'id' => 'enable_auto_update_0',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            ),
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Target Order State'),
                        'name' => 'auto_target_status',
                        'desc' => $this->l('Order state to change to if the trigger category is found.'),
                        'options' => array(
                            'query' => $states,
                            'id' => 'id_order_state',
                            'name' => 'name',
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Trigger Category'),
                        'name' => 'auto_trigger_category',
                        'desc' => $this->l('Default product category that will trigger target order state.'),
                        'options' => array(
                            'query' => $categories, 
                            'id' => 'id_category',
                            'name' => 'name',
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );

        $auto_helper = new HelperForm();
        $auto_helper->table = 'autostatechanger';
        $auto_helper->default_form_languer =
            (int)Configuration::get('PS_LANG_DEFUALT');
        $auto_helper->allow_employee_form_lang =
            (int)Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG');
        $auto_helper->submit_action = 'sc_auto_config_form';
        $auto_helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $auto_helper->token = Tools::getAdminTokenLite('AdminModules');
        $auto_helper->tpl_vars = array(
            'fields_value' => array(
                'enable_auto' => Tools::getValue('enable_auto',
                    Configuration::get('STATE_CHANGER_AUTO_ON')),
                'auto_target_status' => Tools::getValue('auto_target_status',
                    Configuration::get('STATE_CHANGER_AUTO_TARGET_STATUS')),
                'auto_trigger_category' => Tools::getValue('auto_trigger_category',
                    Configuration::get('STATE_CHANGER_AUTO_TRIGGER_CATEGORY')),
                'languages' => $this->context->controller->getLanguages(),
            ),
        );

        $cron_fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Custom CronJob Bulk Update Configuration'),
                    'icon' => 'icon-cog'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow Cron Update'),
                        'name' => 'enable_cron',
                        'desc' => $this->l('Enable cron link bulk update of orders'),
                        'values' => array(
                            array(
                                'id' => 'enable_cron_update_1',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ), array(
                                'id' => 'enable_cron_update_0',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            ),
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Initial Order State'),
                        'name' => 'cron_init_status',
                        'desc' => $this->l('Order state to search through for the trigger category.'),
                        'options' => array(
                            'query' => $states,
                            'id' => 'id_order_state',
                            'name' => 'name',
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Target Order State'),
                        'name' => 'cron_target_status',
                        'desc' => $this->l('Order state to change to if the trigger category is found.'),
                        'options' => array(
                            'query' => $states,
                            'id' => 'id_order_state',
                            'name' => 'name',
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Trigger Category'),
                        'name' => 'cron_trigger_category',
                        'desc' => $this->l('Default product category that will trigger status change.'),
                        'options' => array(
                            'query' => $categories, 
                            'id' => 'id_category',
                            'name' => 'name',
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );

        $cron_helper = new HelperForm();
        $cron_helper->table = 'cronstatechanger';
        $cron_helper->default_form_languer =
            (int)Configuration::get('PS_LANG_DEFUALT');
        $cron_helper->allow_employee_form_lang =
            (int)Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG');
        $cron_helper->submit_action = 'sc_cron_config_form';
        $cron_helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $cron_helper->token = Tools::getAdminTokenLite('AdminModules');
        $cron_helper->tpl_vars = array(
            'fields_value' => array(
                'enable_cron' => Tools::getValue('enable_cron',
                    Configuration::get('STATE_CHANGER_CRON_ON')),
                'cron_init_status' => Tools::getValue('cron_init_status',
                    Configuration::get('STATE_CHANGER_CRON_INIT_STATUS')),
                'cron_target_status' => Tools::getValue('cron_target_status',
                    Configuration::get('STATE_CHANGER_CRON_TARGET_STATUS')),
                'cron_trigger_category' => Tools::getValue('cron_trigger_category',
                    Configuration::get('STATE_CHANGER_CRON_TRIGGER_CATEGORY')),
                'languages' => $this->context->controller->getLanguages(),
            ),
        );

        return $cron_helper->generateForm(array($cron_fields_form))
            .$man_helper->generateForm(array($man_fields_form))
            .$auto_helper->generateForm(array($auto_fields_form));
    }

    private function processConfiguration()
    {
        if (tools::issubmit('sc_man_config_form')) {
            $enabled = boolval(tools::getvalue('enable_man'));
            if ($enabled) {
                $init_status = intval(tools::getvalue('man_init_status'));
                $target_status = intval(tools::getvalue('man_target_status'));
                $trigger_category = intval(tools::getvalue('man_trigger_category'));
                if (isset($init_status)
                    && isset($target_status)
                    && isset($trigger_category)
                    && $init_status
                    && $target_status
                    && $trigger_category
                    && validate::isunsignedint($init_status)
                    && validate::isunsignedint($target_status)
                    && validate::isunsignedint($trigger_category)
                ) {
                    configuration::updatevalue('STATE_CHANGER_MAN_ON', $enabled);
                    configuration::updatevalue('STATE_CHANGER_MAN_INIT_STATUS', $init_status);
                    configuration::updatevalue('STATE_CHANGER_MAN_TARGET_STATUS', $target_status);
                    configuration::updatevalue('STATE_CHANGER_MAN_TRIGGER_CATEGORY', $trigger_category);
                    return 1;
                } else {
                    return -1;
                } 
            } elseif (isset($enabled) && !$enabled) {
                configuration::updatevalue('STATE_CHANGER_MAN_ON', $enabled);
                return 1;
            }
        }

        if (tools::issubmit('sc_auto_config_form')) {
            $enabled = boolval(tools::getvalue('enable_auto'));
            if ($enabled) {
                $target_status = intval(tools::getvalue('auto_target_status'));
                $trigger_category = intval(tools::getvalue('auto_trigger_category'));
                if (isset($target_status)
                    && isset($trigger_category)
                    && $target_status
                    && $trigger_category
                    && validate::isunsignedint($target_status)
                    && validate::isunsignedint($trigger_category)
                ) {
                    configuration::updatevalue('STATE_CHANGER_AUTO_ON', $enabled);
                    configuration::updatevalue('STATE_CHANGER_AUTO_TARGET_STATUS', $target_status);
                    configuration::updatevalue('STATE_CHANGER_AUTO_TRIGGER_CATEGORY', $trigger_category);
                    return 1;
                } else {
                    return -1;
                }
            } elseif (isset($enabled) && !$enabled) {
                configuration::updatevalue('STATE_CHANGER_AUTO_ON', $enabled);
                return 1;
            }
        }

        if (tools::issubmit('sc_cron_config_form')) {
            $enabled = boolval(tools::getvalue('enable_cron'));
            if ($enabled) {
                $init_status = intval(tools::getvalue('cron_init_status'));
                $target_status = intval(tools::getvalue('cron_target_status'));
                $trigger_category = intval(tools::getvalue('cron_trigger_category'));
                if (isset($init_status)
                    && isset($target_status)
                    && isset($trigger_category)
                    && $init_status
                    && $target_status
                    && $trigger_category
                    && validate::isunsignedint($init_status)
                    && validate::isunsignedint($target_status)
                    && validate::isunsignedint($trigger_category)
                ) {
                    configuration::updatevalue('STATE_CHANGER_CRON_ON', $enabled);
                    configuration::updatevalue('STATE_CHANGER_CRON_INIT_STATUS', $init_status);
                    configuration::updatevalue('STATE_CHANGER_CRON_TARGET_STATUS', $target_status);
                    configuration::updatevalue('STATE_CHANGER_CRON_TRIGGER_CATEGORY', $trigger_category);
                    return 1;
                } else {
                    return -1;
                } 
            } elseif (isset($enabled) && !$enabled) {
                configuration::updatevalue('STATE_CHANGER_CRON_ON', $enabled);
                return 1;
            }
        }

        return 0;
    }
}
