<?php

include_once (_PS_MODULE_DIR_ . '/statechanger/tools/Changer.php');

class AdminStateChangerController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->init_status = Configuration::get('STATE_CHANGER_MAN_INIT_STATUS');
        $this->target_status = Configuration::get('STATE_CHANGER_MAN_TARGET_STATUS');
        $this->trigger_category = Configuration::get('STATE_CHANGER_MAN_TRIGGER_CATEGORY');
        $this->man_enabled = Configuration::get('STATE_CHANGER_MAN_ON');
        parent::__construct();
    }
    
    public function createTemplate($tpl_name) {
        if (file_exists($this->getTemplatePath() . $tpl_name) && $this->viewAccess()) {
            return $this->context->smarty->createTemplate($this->getTemplatePath() . $tpl_name, $this->context->smarty);
        }
        return parent::createTemplate($tpl_name);
    }

    public function initContent(){
        parent::initContent();

        $settings_link = $this->context->link->getAdminLink('AdminModules') . '&configure=' . Tools::safeOutput($this->module->name);

        $this->setTemplate('view.tpl');

        $init_status_name = (new OrderState($this->init_status))->name[1];
        $target_status_name = (new OrderState($this->target_status))->name[1];
        $trigger_category_name = (new Category($this->trigger_category))->name[1];

        $this->context->smarty->assign(array(
            'settings_link' => $settings_link,
            'man_enabled' => $this->man_enabled,
            'init_status' => $init_status_name,
            'target_status' => $target_status_name,
            'trigger_category' => $trigger_category_name,
        ));
    }

    public function postProcess()
    {
        if (Tools::isSubmit('sc_action')) {
            $statuses_changed = Changer::manProcess($this->init_status, $this->target_status, $this->trigger_category);
            if ($statuses_changed > 0) {
                // wipe off 'orders todo array'
                $new_orders = array();
                Configuration::updateValue('STATE_CHANGER_NEW_ORDERS', serialize($new_orders));
            } 
            $this->context->smarty->assign('statuses_changed', $statuses_changed);
        }
    }
    

}
